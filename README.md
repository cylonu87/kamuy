# Kamuy is an unofficial android client for MyAnimeList, Kitsu, AniList, Anime-Planet, LiveChart and MyDramaList to track your anime, manga and drama.

## Features
* Manage your anime and manga: progress, status, rate.
* Discover anime: top, popular, upcoming, airing, just added, seasonal.
* Search anime and manga by name.
* Manage your lists: Plan to watch/read, Watching/reading, On-hold, Dropped, Completed.
* MyAnimeList, Kitsu, AniList, Anime-Planet, LiveChart and MyDramaList sync.
* Recommandations.
* Light or dark theme.
* Libraries can be accessed offline.
* List or grid view.

## Requirement:
* Android 4.1 or higher (API 16).

## Downloads:
* [Github](https://cylonu87.github.io/androidapps/)
* [Wix](https://cyberneticlifeform.wixsite.com/cylonu87/kamuy)
* [Bitbucket](https://bitbucket.org/cylonu87/kamuy/downloads/)